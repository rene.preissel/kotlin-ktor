# Fazit
## Kotlin
 * Pragmatische Verbesserungen für die typischen  Java-Schmerzpunkte
 * Syntax fühlt sich modern an und macht Spaß
 * Sinnvolle und bewährte Ideen aus anderen Sprachen übernommen
 * Gut geeignet für interne DSLs 
 * Java würde so aussehen, wenn es 2010 entwickelt worden wäre<br/>
    -> Java übernimmt immer mehr Features von Kotlin (Raw Strings, Data Classes)
    
## Ktor
 * Kompaktes Framework für die Entwicklung von Webanwendungen / -services
 * Funktionaler Stil und einfache DSLs erlauben kompakten lesbaren Code und einfache Erweiterbarkeit
 * Ressourcenschonend und trotzdem mit bewährten Programmiermodel dank Koroutinen      