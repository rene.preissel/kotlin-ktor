rootProject.name = "kotlin-ktor"

enableFeaturePreview("GRADLE_METADATA")

include(":p05-chat-mpp")

pluginManagement {
    plugins {
        val kotlinVersion = "1.3.50"

        id("org.jetbrains.kotlin.jvm") version kotlinVersion
        id("org.jetbrains.kotlin.plugin.serialization") version kotlinVersion
    }
}

