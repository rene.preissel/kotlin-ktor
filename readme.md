## Kotlin und Ktor (JUG Saxony Day 2019)

René Preißel (@RenePreissel, rene.preissel@etosquare.de)

### Ziel
* [Kotlin](https://kotlinlang.org/) und [Ktor (Webframework)](https://ktor.io/) in einer Stunde mit Code-Beispielen vorstellen    
* Fokus liegt auf den __Unterschieden__ zu Java und den __Innovationen__ 
* `#NoSlides`

### Voraussetzungen
* __Java__ Kenntnisse und grundlegende Erfahrungen in der __Webentwicklung__ (HTML | REST | Servlets | Spring | ...) 

### Code
* Gitlab: https://gitlab.com/rene.preissel/kotlin-ktor

### Agenda
* [Hello World](src/main/kotlin/de/e2/ktor/p01_helloworld/HelloWorld.md)
* [HTML / REST](src/main/kotlin/de/e2/ktor/p02_adder/HTML_REST.md)
* [Koroutinen](src/main/kotlin/de/e2/ktor/p03_collage/Koroutinen.md)
* [Websockets](src/main/kotlin/de/e2/ktor/p04_echo/Websockets.md)
* [Bonus: Multiplatform Project](p05-chat-mpp/Multiplatform.md)
* [Fazit](Fazit.md)

