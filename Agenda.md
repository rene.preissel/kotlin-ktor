### Hello World
* [Einstieg Kotlin und Ktor](src/main/kotlin/de/e2/ktor/p01_helloworld/HelloWorld_1.kt)
* [Lambdas](src/main/kotlin/de/e2/ktor/p01_helloworld/HelloWorld_2a_Repeat.kt) 
            ([B](src/main/kotlin/de/e2/ktor/p01_helloworld/HelloWorld_2b_Repeat.kt))
* [Nullables](src/main/kotlin/de/e2/ktor/p01_helloworld/HelloWorld_3a_Nullable.kt) 
            ([B](src/main/kotlin/de/e2/ktor/p01_helloworld/HelloWorld_3b_If_Expression.kt),
             [C](src/main/kotlin/de/e2/ktor/p01_helloworld/HelloWorld_3c_Fragezeichen.kt),
             [D](src/main/kotlin/de/e2/ktor/p01_helloworld/HelloWorld_3d_Elvis.kt),
             [E](src/main/kotlin/de/e2/ktor/p01_helloworld/HelloWorld_3e_Elvis_Mit_Throw.kt)) 
* [Extensions](src/main/kotlin/de/e2/ktor/p01_helloworld/HelloWorld_4a_Eigene_Funktion_Mit_Return.kt) 
            ([B](src/main/kotlin/de/e2/ktor/p01_helloworld/HelloWorld_4b_Funktions_Statement.kt),
             [C](src/main/kotlin/de/e2/ktor/p01_helloworld/HelloWorld_4c_Extension_Funktion.kt))
* [Ktor Module](src/main/kotlin/de/e2/ktor/p01_helloworld/HelloWorld_5a_Ktor_Modul.kt) 
            ([B](src/main/kotlin/de/e2/ktor/p01_helloworld/HelloWorld_5b_Ktor_mehrere_Module.kt),
             [C](src/test/kotlin/de/e2/ktor/p01_helloworld/HelloWorld_5a_Ktor_ModulTest.kt)) 
             
### [Adder](src/main/kotlin/de/e2/ktor/p02_adder/img/Adder.png)                                                     
* [Optional: Raw Strings](src/main/kotlin/de/e2/ktor/p02_adder/Adder_1_Raw_Strings.kt)
* [HTML DSL](src/main/kotlin/de/e2/ktor/p02_adder/Adder_2a_Html_DSL.kt) 
            ([B](src/main/kotlin/de/e2/ktor/p02_adder/Adder_2b_Html_DSL_mit_Operator.kt),
             [Optional: C](src/main/kotlin/de/e2/ktor/p02_adder/Adder_2c_Html_DSL_eigene_Komponente.kt))
* [Optional: Locations und Objekte](src/main/kotlin/de/e2/ktor/p02_adder/Adder_3a_Locations.kt) 
            ([B](src/main/kotlin/de/e2/ktor/p02_adder/Adder_3b_Objekte.kt))
* [Rest-API](src/main/kotlin/de/e2/ktor/p02_adder/Adder_4a_Rest_Api.kt) 
            ([B](src/main/kotlin/de/e2/ktor/p02_adder/Adder_4a_Rest_Api.rest),
             [C](src/main/kotlin/de/e2/ktor/p02_adder/Adder_4b_Methoden_Data.kt),
             [D](src/main/kotlin/de/e2/ktor/p02_adder/Adder_4c_API_mit_Cache.kt),
             [E](src/main/kotlin/de/e2/ktor/p02_adder/Adder_4c_Api_mit_Cache.rest))
             
### [Collage](src/main/kotlin/de/e2/ktor/p03_collage/img/turtle.png)             
* [Ktor-Client und Koroutinen](src/main/kotlin/de/e2/ktor/p03_collage/Collage_1_coroutine.kt)
            ([Blocking APIs](src/main/kotlin/de/e2/ktor/p03_collage/img/blocking.png),
             [Funktion](src/main/kotlin/de/e2/ktor/p03_collage/img/routine.png),
             [Koroutine](src/main/kotlin/de/e2/ktor/p03_collage/img/coroutine.png))
* [Parallelität und Koroutinen](src/main/kotlin/de/e2/ktor/p03_collage/Collage_2_coroutine_async.kt)

### Echo Service
* [Einfacher Websocket](src/main/kotlin/de/e2/ktor/p04_echo/echo_1_websocket/EchoServer.kt) 
            ([Client](src/main/kotlin/de/e2/ktor/p04_echo/echo_1_websocket/EchoClient.kt),
             [Klassisch mit Typescript](src/main/kotlin/de/e2/ktor/p04_echo/echo_1_websocket/Websocket.ts))
* [Broadcasting](src/main/kotlin/de/e2/ktor/p04_echo/echo_2_broadcast/EchoBroadcastServer.kt) 
            ([Client](src/main/kotlin/de/e2/ktor/p04_echo/echo_2_broadcast/EchoBroadcastClient.kt))

### Optional: Chat (Multiplatform-Projekt)
* [Common Chat Client API](p05-chat-mpp/src/commonMain/kotlin/de/e2/ktor/multiplatform/ChatClient.kt)
* [Kotlin Chat Client](p05-chat-mpp/src/jvmMain/kotlin/de/e2/ktor/multiplatform/ChatClientKotlin.kt)
* [HTML Chat Client](p05-chat-mpp/src/jvmMain/resources/index.html) 
            ([Javascript](p05-chat-mpp/src/jsMain/kotlin/de/e2/ktor/multiplatform/ChatClientJs.kt))
* [Logging](p05-chat-mpp/src/commonMain/kotlin/de/e2/ktor/multiplatform/Logging.kt)
            ([Kotlin](p05-chat-mpp/src/jvmMain/kotlin/de/e2/ktor/multiplatform/LoggingJvm.kt),
            [Javascript](p05-chat-mpp/src/jsMain/kotlin/de/e2/ktor/multiplatform/LoggingJs.kt)) 
 
### [Fazit](Fazit.md) 