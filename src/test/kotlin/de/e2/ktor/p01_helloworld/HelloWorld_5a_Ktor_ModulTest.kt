package de.e2.ktor.p01_helloworld

import io.ktor.application.Application
import io.ktor.http.HttpMethod
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class HelloModuleTest {
    @Test
    fun `hello5 returns five times world`()
            = withTestApplication(Application::helloModule) {
        val testCall = handleRequest(HttpMethod.Get, "/hello/5")
        val content = testCall.response.content

        assertNotNull(content)
        assertEquals(5, "World".toRegex().findAll(content).count())
    }
}