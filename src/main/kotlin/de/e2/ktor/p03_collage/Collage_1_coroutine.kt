package de.e2.ktor.p03_collage

import com.jayway.jsonpath.JsonPath
import de.e2.ktor.p01_helloworld.required
import de.e2.ktor.util.combineImages
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.response.HttpResponse
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.response.respondOutputStream
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.coroutines.io.jvm.javaio.toInputStream
import java.awt.image.BufferedImage
import java.io.IOException
import javax.imageio.ImageIO

fun Application.collage() {
    val ktorClient = HttpClient()
    routing {
        get("/collage/{query}") {
            val query = call.parameters.required("query")
            val count = call.parameters["count"]?.toInt() ?: 20

            val urls: List<String> = ktorClient.requestImageUrls(query, count)
            val images: List<BufferedImage> = urls.map { ktorClient.requestImageData(it) }
            val image = combineImages(images)

            call.respondOutputStream(ContentType.Image.PNG) {
                ImageIO.write(image, "png", this)
            }
        }
    }
}

//<editor-fold desc="Request-Funktionen" defaultstate="collapsed">
suspend fun HttpClient.requestImageUrls(query: String, count: Int = 20): List<String> {
    val json = get<String>("https://api.qwant.com/api/search/images?offset=0&t=images&uiv=1&q=$query&count=$count")
    return JsonPath.read<List<String>>(json, "$..thumbnail").map { "https:$it" }
}

suspend fun HttpClient.requestImageData(imageUrl: String): BufferedImage {
    val httpResponse = get<HttpResponse>(imageUrl)
    if (httpResponse.status == HttpStatusCode.OK) {
        return ImageIO.read(httpResponse.content.toInputStream())
    }
    throw IOException("Wrong status code ${httpResponse.status}")
}
//</editor-fold>

fun main() {
    val server = embeddedServer(Netty, port = 8080, module = Application::collage)
    println("Open with: http://localhost:8080/collage/turtle?count=12")
    server.start(wait = true)
}
