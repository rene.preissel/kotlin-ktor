## Koroutinen             
* [Ktor-Client und Koroutinen](Collage_1_coroutine.kt)
    * [Blocking APIs](img/blocking.png)
    * [Funktion](img/routine.png)
    * [Koroutine](img/coroutine.png)
* [Parallelität und Koroutinen](Collage_2_coroutine_async.kt)

---

### Beispiel: Collage

![](img/turtle.png)
