# Parallelität
* Koroutinen sind *Sequentiell per Default*
* Parallelität muss explizit gestartet werden, z.B. mit `async`
    * Das Ergebnis ist ein `Deferred` (Future, Promise) auf das mit `await() / awaitAll()` gewartet werden kann
     