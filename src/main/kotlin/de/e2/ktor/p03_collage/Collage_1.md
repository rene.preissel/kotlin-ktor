# Ktor Http-Client
* Ktor kommt mit einem Http-Client, um andere HTTP-Backends aufzurufen

# Problem mit Blocking Aufrufen
* Hoher Resourcenverbrauch (Threads blockieren und belegen Speicher)
    * Dafür einfaches Programmiermodel
* Alternativen sind Callbacks oder Reaktive Streams (Funktionale Operatoren)
    * Besserer Ressourcenverbrauch - aufwändigeres Programmiermodel     

# Koroutinen
* Koroutinen sind in der Lage den aktuellen Thread freizugeben und genau an der gleichen Stelle wieder fortzusetzen, wenn ein Ereignis eintrifft.
* `suspend` als Keyword um Koroutinen zu definieren
* Koroutinen können nur innerhalb von anderen Koroutinen aufgerufen werden
    * Wird eine untere Koroutine suspendiert, wird auch die aufrufende Koroutine suspendiert
    * Am Beginn der Aufrufkette muss explizit ein `CoroutineScope` erzeugt werden
* Ktor erzeugt für jeden Request eine Koroutine und ruft den Handler mit `suspend` auf
    * Deswegen können innerhalb des Handlers weitere Koroutinen aufgerufen werden
* Der Ktor-HttpClient suspendiert sich automatisch solange auf die Daten des anderen Systems gewartet wird.
     