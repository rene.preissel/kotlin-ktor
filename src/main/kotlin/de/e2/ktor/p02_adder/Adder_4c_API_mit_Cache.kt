package de.e2.ktor.p02_adder.c

import com.fasterxml.jackson.databind.JsonMappingException
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.jackson
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.coroutines.delay
import org.slf4j.LoggerFactory

data class AddRequest(val arg1: Int, val arg2: Int)
data class AddResult(val sum: Int)

fun Application.adderApiCache() {
    val cache: Map<AddRequest, AddResult> = mapOf(
        Pair(AddRequest(0, 1), AddResult(1)), // normaler Konstruktor

        AddRequest(1, 0).to(AddResult(1)),

        AddRequest(1, 1) to AddResult(2) //Infix Funktion
    )

    val logger = LoggerFactory.getLogger("server")

    //<editor-fold desc="Json - Details" defaultstate="collapsed">
    install(ContentNegotiation) {
        jackson()
    }

    install(StatusPages) {
        exception<JsonMappingException> {
            call.respond(HttpStatusCode.UnprocessableEntity)
        }
    }
    //</editor-fold>

    routing {
        post("/add") {
            val req: AddRequest = call.receive()

            logger.info("Request $req")

            val result = cache.getOrElse(req) {
                delay(2000) // Nur für Demo
                AddResult(req.arg1 + req.arg2)
            }

            call.respond(result)
        }
    }
}

fun main() {
    val server = embeddedServer(Netty, port = 8080, module = Application::adderApiCache)
    server.start(wait = true)
}