package de.e2.ktor.p02_adder.b

import com.fasterxml.jackson.databind.JsonMappingException
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.jackson
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.slf4j.LoggerFactory

class AddRequest(val arg1: Int, val arg2: Int) {
    fun sum() =  arg1 + arg2

    //Alternativ eine eigenes Property
    val sum: Int = arg1 + arg2
}

class AddResult(val sum: Int)

fun Application.adderApi() {
    val logger = LoggerFactory.getLogger("server")

    //<editor-fold desc="Json - Details" defaultstate="collapsed">
    install(ContentNegotiation) {
        jackson()
    }

    install(StatusPages) {
        exception<JsonMappingException> {
            call.respond(HttpStatusCode.UnprocessableEntity)
        }
    }
    //</editor-fold>

    routing {
        post("/add") {
            val req: AddRequest = call.receive()

            logger.info("Request ${req.arg1} ${req.arg2}")

            call.respond(AddResult(req.sum))
        }
    }
}

fun main() {
    val server = embeddedServer(Netty, port = 8080, module = Application::adderApi)
    server.start(wait = true)
}