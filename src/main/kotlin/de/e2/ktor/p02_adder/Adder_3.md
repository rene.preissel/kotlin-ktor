# Eigene Klassen 
* Klassen können kompakt definiert werden: `class Homepage`
    * Alle Klassen sind per Default `final`. `open` zum öffnen angeben.
    * Annotations wie gehabt in Java
* Der primäre Konstruktor wird gleich nach dem Klassennamen definiert
    * Parameter mit `val` und `var` werden automatisch zu Properties         
* Methoden werden einfach im Klassenkörper mit `fun` definiert und können natürlich auf die Properties zugreifen
    * Alle Methoden sind per Default `final`. `open`zum öffnen angeben.

# Ktor-Locations
* Locations ist eine Erweiterung von Ktor um typsichere Routen zu definieren
    * Erweiterungen müssen mittels `install()` registriert werden
    * Anstelle des Strings wird die Location mit Annoations angegeben
* Das Handler-Lambda bekommt die passende Location-Instanz inklusive gefundener Request-Parameter übergeben
* Mit Hilfe von `call.locations.href(...)` können URLs erzeugt werden.
    * Objekte werden in Kotlin ohne `new` angelegt: `HomePage()`
* Die neue Funktion `get` und das neuen Property `call.locations` wurde mittels Extension-Funktion/Property in die DSL eingebracht 


# Properties
* Eigenes Property mit eigenem Attribut wird im Klassenkörper mittels `val` oder `var`definiert
 * Es weren automatisch ein Datenfeld und Getter bzw. Seter definiert
 * Bei der Initialisierung kann auf die Konstruktor-Parameter zugegriffen werden
* Es ist auch möglich ein Property ohne Datenfeld und nur mit Getter- bzw. Setter zu definieren.  

# Objekte
+ Wird von einer Klasse nur ein Objekt benötigt (Singleton), wird diese per `object` definiert
* Der Zugriff erfolgt über den Klassennamen
    * `Netty` ist ein Objekt
* Wird sowohl ein einziges Objekt für eine Klasse und Instanzen benötigt, dann gibt es `companion`-Objekte
    * Über den Klassennamen erfolgt der Zugriff auf das Companion-Objekt
    * `Location` hat ein Companion-Objekt    


