# HMTL und REST
                                                                      
* [HTML DSL](Adder_2a_Html_DSL.kt) 
            ([B](Adder_2b_Html_DSL_mit_Operator.kt),
             [C](Adder_2c_Html_DSL_eigene_Komponente.kt))
* [Rest-API](Adder_4a_Rest_Api.rest) 
            ([B](Adder_4a_Rest_Api.kt),
             [C](Adder_4b_Methoden_Data.kt))

---

### Beispiel: Adder

![](img/Adder.png)
            
            