@file:UseExperimental(KtorExperimentalLocationsAPI::class)

package de.e2.ktor.p02_adder.a

import de.e2.ktor.p02_adder.addArgument
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.html.respondHtml
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location
import io.ktor.locations.Locations
import io.ktor.locations.get
import io.ktor.locations.locations
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.html.ButtonType
import kotlinx.html.FormMethod
import kotlinx.html.a
import kotlinx.html.body
import kotlinx.html.button
import kotlinx.html.div
import kotlinx.html.form
import kotlinx.html.h1

@Location("/")
class HomePage


@Location("/add")
class AdderPage(val arg1: Int, val arg2: Int) {
    val summe = arg1 + arg2
}

fun Application.adderLocation() {
    install(Locations)
    routing {
        get<HomePage>() {
            call.respondHtml {
                body {
                    h1 { text("Adder") }
                    form(action = "add", method = FormMethod.get) {
                        addArgument(label = "Arg1", name = "arg1")
                        addArgument(label = "Arg2", name = "arg2")
                        button(type = ButtonType.submit) { text("Add") }
                    }
                }
            }
        }

        get<AdderPage>() { adderPage ->
            call.respondHtml {
                body {
                    h1 { text("Result") }
                    div {
                        text("${adderPage.arg1} + ${adderPage.arg2} = ${adderPage.summe} ")
                    }
                    div {
                        a(href = call.locations.href(HomePage())) { text("Go to HomePage") }
                    }
                }
            }
        }
    }
}

@KtorExperimentalLocationsAPI
fun main() {
    val server = embeddedServer(Netty, port = 8080, module = Application::adderLocation)
    server.start(wait = true)
}