package de.e2.ktor.p02_adder

import de.e2.ktor.p01_helloworld.required
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

fun Application.adderRawString() {
    routing {
        get("/") {
            call.respondText(
                contentType = ContentType.Text.Html,
                text = """
                        <h1>Adder</h1>
                        <form action="add" method="get">
                            <label> 
                                Arg1
                                <input type="text" name="arg1"/>
                            </label>
                            <label> 
                                Arg2
                                <input type="text" name="arg2"/>
                            </label>
                            <button type="submit">Add</button>
                        </form>
                       """
            )
        }

        get("/add") {
            val arg1 = call.parameters.required("arg1").toInt()
            val arg2 = call.parameters.required("arg2").toInt()

            call.respondText(
                contentType = ContentType.Text.Html,
                text = """
                        <h1>Result</h1>
                        <div> 
                            $arg1 + $arg2 = ${arg1 + arg2} 
                        </div>
                        <div>
                            <a href="/">Go to Homepage</a>
                        </div>
                       """
            )
        }
    }
}

fun main() {
    val server = embeddedServer(Netty, port = 8080, module = Application::adderRawString)
    server.start(wait = true)
}