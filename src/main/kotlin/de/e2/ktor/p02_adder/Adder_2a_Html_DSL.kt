package de.e2.ktor.p02_adder

import de.e2.ktor.p01_helloworld.required
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.html.respondHtml
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.html.ButtonType
import kotlinx.html.FormMethod
import kotlinx.html.a
import kotlinx.html.body
import kotlinx.html.button
import kotlinx.html.div
import kotlinx.html.form
import kotlinx.html.h1
import kotlinx.html.label
import kotlinx.html.textInput

fun Application.adderHtmlDsl() {
    routing {
        get("/") {
            call.respondHtml {
                body {
                    h1 { text("Adder") }
                    form(action = "add", method = FormMethod.get) {
                        label {
                            text("Arg1")
                            textInput(name = "arg1")
                        }
                        label {
                            text("Arg2")
                            textInput(name = "arg2")
                        }
                        button(type = ButtonType.submit) {
                            text("Add")
                        }
                    }
                }
            }
        }

        get("/add") {
            val arg1 = call.parameters.required("arg1").toInt()
            val arg2 = call.parameters.required("arg2").toInt()

            call.respondHtml {
                body {
                    h1 { text("Result") }
                    div {
                        text("$arg1 + $arg2 = ${arg1 + arg2}")
                    }
                    div {
                        a(href = "/") {
                            text("Go to HomePage")
                        }
                    }
                }
            }
        }
    }
}

fun main() {
    val server = embeddedServer(Netty, port = 8080, module = Application::adderHtmlDsl)
    println("Open with: http://localhost:8080")
    server.start(wait = true)
}