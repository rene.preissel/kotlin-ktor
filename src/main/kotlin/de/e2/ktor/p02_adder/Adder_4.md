# Rest-Services
* Ktor unterstützt natürlich auch Rest-Services
* Mit der Funktion `receive` kann der Reqeust geparst werden und in ein Kotlin-Objekt umgewandelt werden
* Ergebnisse werden mit `call.respond(..)` in Json umgewandelt
* Für die Umsetzung muss ein weiteres Feature `ContentNegotiation` installiert und konfiguriert werden.
* Damit beim fehlerhaften Parsen ein vernünftiger HTTP-Code (z.B. 422) kommt muss das Feature `StatusPages` installiert und konfiguriert werden.   

# Inline Reified
* Die Funktion `receive` ist als `inline`definiert worden und hat einen generischen `reified` Typparameter
    * Dadurch wird es möglich zur Laufzeit auf das Klassenobjekt zuzugreifen und Type-Erasure zu umgehen. 
    
# Data-Klassen
* Klassen  die mit `data` definiert werden bekommen eine Reihe nützlicher Funktionen, z.B. 
    * eine `toString`-Methode
    * `equals` und `hashCode`
    
# Sonstiges
* Der doppelte Gleicheitsoperator `==` ruft `equals` auf. `===` überprüft auf Identität.
* Kotlin unterstützt Infix-Funktionen (auch eigene)
    * `AddRequest(0, 1) to AddResult(1)` erzeugt ein `Pair`-Objekt       
    