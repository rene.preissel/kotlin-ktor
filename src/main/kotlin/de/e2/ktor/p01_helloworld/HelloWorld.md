### Hello World
* [Einstieg Kotlin und Ktor](HelloWorld_2a_Repeat.kt) 
            ([B](HelloWorld_2b_Repeat.kt))
* [Nullables](HelloWorld_3a_Nullable.kt) 
            ([B](HelloWorld_3b_Fragezeichen.kt),
             [C](HelloWorld_3c_If_Expression.kt),
             [D](HelloWorld_3d_Elvis.kt),
             [E](HelloWorld_3e_Elvis_Mit_Throw.kt)) 
* [Extensions](HelloWorld_4a_Eigene_Funktion_Mit_Return.kt) 
            ([B](HelloWorld_4b_Funktions_Statement.kt),
             [C](HelloWorld_4c_Extension_Funktion.kt))
* [Ktor Module](HelloWorld_5a_Ktor_Modul.kt)              
