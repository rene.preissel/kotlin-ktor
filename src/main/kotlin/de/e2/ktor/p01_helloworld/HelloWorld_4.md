# Funktionen und Extension-Funktionen

* Funktionen die nur aus einem statement bestehen können mittels `=` direkt hinter der Funktionsdefinition geschrieben werden  
* Vorhandene Typen können um Methoden erweitert werden: __Extensions Funktion__
    * Vermeidung von statischen Hilfsfunktionen. 
    * Compiler erzeugt unter der Haube auch statische Funktionen.
    * Kein Zugriff auf private Variablen und keine neuen Properties möglich.
    * Innerhalb der Funktion ist `this` vom "richtigen" Typ
* Der Aufruf einer Extension Funktion sieht wie bei einer "normalen" Methode aus.
    * müssen explizit importiert werden
        
