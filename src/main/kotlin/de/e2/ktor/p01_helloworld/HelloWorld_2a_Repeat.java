package de.e2.ktor.p01_helloworld;

import java.util.function.Consumer;

public class HelloWorld_2a_Repeat {

    public static void main(String[] args) {
        repeat(5, index -> {
            System.out.println("World " + index);
        });
    }

    public static void repeat(int times, Consumer<Integer> action) {
        for (int i = 1; i <= times; i++) {
            action.accept(i - 1);
        }
    }
}
