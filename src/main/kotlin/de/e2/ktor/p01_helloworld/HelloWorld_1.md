# Ktor - Was ist das?
* Webcontainer/-server für Webanwendungen und REST-Services

# Kotlin - erster Eindruck
* `package` und `import` können genauso wie in Java benutzt werden
    * __Aber__: Verzeichnisstruktur muss nicht mit Package-Struktur übereinstimmen
* __Globale Funktionen__ werden unterstützt mit `fun` definiert (gilt auch für Klassen-Methoden) 
* `main()` ist globale Einstiegsfunktion ohne `args` als Parameter
* 'val' definiert `final` Variablen. Der Datentyp wird meistens automatisch bestimmt.
* Named Parameter und Default-Werte für Parameter sind möglich
* *Trailing Lambdas* werden nach den Parameterklammern übergeben
    
