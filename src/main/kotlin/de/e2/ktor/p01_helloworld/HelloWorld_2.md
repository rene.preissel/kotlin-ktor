# Functions, Function Types und Lambdas

* Parameter Typen werden hinter den Namen getrennt durch `:` angegeben
* Der Return Type steht hinter der Funktionsdefinition
    * `Unit` steht für etwas, was nicht relevant ist (entspricht etwa `void`)
    * `Unit` kann als Return-Typ auch weggelassen werden, ist der Default
* Funktionstypen werden direkt unterstützt: `(Int) -> Unit`
    * Kotlin unterscheidet nicht zwischen primitiven und Objekttypen: `Int`. Compiler optimiert das aber.
    * Funktionsinstanzen (Lambdas) werden aufgerufen wie normale Funktionen: `action(...)`
* Funktionstypen als letzter Parameter einer Funktion, können beim Aufruf als *trailing* Lambda hinter den Parameterklammern übergeben werden.
    * Parameter werden innerhalb des Lambdas spezifiziert: `{ index -> ...}`
    * Wenn es nur einen Parameter gibt, kann der Name auch weggelassen werden und ist automatisch `it`
    * Innerhalb von Lambdas kann der `this `-Zeiger redefiniert werden     

# Sonstiges
* Es gibt Ranges: `1..times`, wobei das Ende inklusive ist.
* `for`-Schleifen gibt es nur in der `in`-Form, d.h. kein `for(;;)`
* Innerhalb von Strings kann mittels `$` auf Variablen zugegriffen werden   
* Auf Properties kann direkt ohne explizizen Getter zugegriffen werden: `call.respondTextWriter(..)`
    * intern wird trotzdem der Getter/Setter benutzt
* Das Property `call` erlaubt den Zugriff auf den aktuellen Request     