package de.e2.ktor.p01_helloworld

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.response.respondText
import io.ktor.response.respondTextWriter
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

fun Application.helloSimpleModule() {
    routing {
        get("/hello") {
            call.respondText("World")
        }
    }
}

fun Application.helloCountModule() {
    routing {
        get("/hello/{count}") {
            val countAsInt: Int = call.parameters.required("count").toInt()

            call.respondTextWriter {
                repeat(countAsInt) {
                    write("World $it\n")
                }
            }
        }
    }
}

fun main() {
    val server = embeddedServer(Netty, port = 8080) { // this ist Application
        helloSimpleModule()
        helloCountModule()
    }
    server.start(wait = true)
}