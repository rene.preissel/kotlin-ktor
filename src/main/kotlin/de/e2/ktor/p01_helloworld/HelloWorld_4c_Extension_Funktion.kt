package de.e2.ktor.p01_helloworld

import io.ktor.application.call
import io.ktor.http.Parameters
import io.ktor.response.respondTextWriter
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

fun Parameters.required(name: String) =
    this[name] ?: throw IllegalStateException("Parameter $name is required")


fun main() {
    val server = embeddedServer(Netty, port = 8080) {
        routing {
            get("/hello/{count}") {

                val countAsInt: Int = call.parameters.required("count").toInt()

                call.respondTextWriter {
                    repeat(countAsInt) {
                        write("World $it\n")
                    }
                }
            }
        }
    }
    println("Open with: http://localhost:8080/hello/3")
    server.start(wait = true)
}