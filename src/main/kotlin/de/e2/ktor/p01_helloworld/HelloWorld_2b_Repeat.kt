package de.e2.ktor.p01_helloworld.b

import io.ktor.application.call
import io.ktor.response.respondTextWriter
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty


fun main() {
    val server = embeddedServer(Netty, port = 8080) {
        routing {
            get("/hello5") {
                call.respondTextWriter {
                    repeat(5) {
                        write("World $it\n")
                    }
                }
            }
        }
    }
    println("Open with: http://localhost:8080/hello5")
    server.start(wait = true)
}

fun repeat(times: Int, action: (Int) -> Unit) {
    for (index in 1..times) {
        action(index - 1)
    }
}