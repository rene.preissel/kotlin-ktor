# Nullables, Ktor-Parameter, Vars
* Normale Datentypen sind nicht Nullable
    * Der Compiler stellt sicher, das keine Null-Zuweisungen möglich sind.
* Durch anhängen eines `?` wird jeder Typ zu einen Nullable-Typ
* In Ktor werden Path-Parameter direkt in der Routing-URL angegeben
    * `call.parameters` erlaubt den Zugriff auf die Path-Parameter (und auch auf Query-Parameter: `?count=11`)
    * Kotlin erlaubt das redefinieren von __vorhandenen Operatoren__, z.B. `[]` für Parameter/Maps
* Das Ergebnis einer Parameter-Suche ist ein Nullable-String    
    * Vor dem Zugriff auf diesen String muss überprüft werden, ob dieser nicht `null`ist
    * Nach der Überprüfung weis der Compiler das und castet automatisch auf den Nicht-Nullable-Type: *Smart Cast*    
* Veränderbare Variablen werden mit `var` definiert
* `if` kann als Expression genutzt werden, d.h. liefert ein Ergebnis zurück
    * den tertiären Operator `?` gibt es in Kotlin nicht
    * kann auch einzeilig geschrieben werden
* Die kompakte Schreibweise beim Zugriff auf Nullables geht mit dem `?`-Operator
    * Führt den nachfolgenden Zugrifff nur aus, wenn die linke Seite nicht `null` ist, liefert ansonsten `null` zurück
* Der Elvis-Operator `?:` liefert die linke Seite zurück, wenn diese Nicht-Null ist, ansonsten die rechte Seite.
    * Auf der rechten seite kann auch `return` oder `throw` stehen