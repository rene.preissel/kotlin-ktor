package de.e2.ktor.p04_echo.echo_2_broadcast

import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.readText
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.util.KtorExperimentalAPI
import io.ktor.websocket.WebSockets
import io.ktor.websocket.webSocket
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

@UseExperimental(FlowPreview::class, KtorExperimentalAPI::class, ExperimentalCoroutinesApi::class)
fun Application.broadcastWebsocket() {
    val logger = LoggerFactory.getLogger("server")
    install(WebSockets)
    val broadcastChannel = BroadcastChannel<String>(1)

    routing {
        webSocket("/broadcast") {
            logger.info("Connected")

            val subscriptionChannel = broadcastChannel.openSubscription()
            try {

                //<editor-fold desc="->Broadcast-Job" defaultstate="collapsed">
                launch {
                    for (message in subscriptionChannel) {
                        outgoing.send(Frame.Text(message))
                    }
                }
                //</editor-fold>

                incoming
                    .consumeAsFlow()
                    .mapNotNull { it as? Frame.Text }
                    .map { it.readText() }
                    .collect { text ->
                        logger.info("Receive message $text")
                        broadcastChannel.send(text)
                    }
            } finally {
                subscriptionChannel.cancel()
            }

            logger.info("Disconnect")
        }
    }
}

fun main() {
    val server = embeddedServer(Netty, port = 8080, module = Application::broadcastWebsocket)
    server.start(wait = true)
}