## Websockets
* [Klassischer Websocket mit Spring](echo_1_websocket/SpringWebSocketHandler.kt) 
    * [Ktor und Koroutinen](echo_1_websocket/EchoServer.kt)
* [Websocket Client](echo_1_websocket/EchoClient.kt)
    * [Klassisch mit TypeScript](echo_1_websocket/Websocket.ts)
* [Broadcasting Server](echo_2_broadcast/EchoBroadcastServer.kt) 
    * [Client](echo_2_broadcast/EchoBroadcastClient.kt)
    
---
    
![](img/Websocket.png)    
