function connectWebSocket() {
    this.socket = new WebSocket('ws://localhost:8080/echo');

    this.socket.onopen = () => {
        console.log('Websocket connected');
        //...
    };

    this.socket.onclose = () => {
        console.log('Websocket closed');
    };

    this.socket.onmessage = (mev: MessageEvent) => {
        //...
    };
}